<?php
namespace Lib;

require __DIR__ . '/../vendor/autoload.php';

use Pdp\Cache;
use Pdp\CurlHttpClient;
use Pdp\Manager;
use Pdp\Rules;

class Parser {

	private $m;

	public function __construct() {
		$this->m = new Manager(new Cache(), new CurlHttpClient());
	}

	public function suffix($d) {
		$ps = $this->m->getRules()->getPublicSuffix($d);

		return $ps->getContent();
	}

	public function domain($d) {
		$domain = $this->m->getRules()->resolve($d)->getRegistrableDomain();

		return $domain;
	}
}

?>
