<?php
declare(strict_types=1);

namespace Lib\Tests;

require __DIR__ . '/../src/Parser.php';

use PHPUnit\Framework\TestCase;
use Lib\Parser;

final class ThingTest extends TestCase {
    
    public function testCanGetPublicSuffix(): void {
        $parser = new Parser();
        $d = $parser->suffix("domain.com");
        
        $this->assertTrue($d == "com");
    }

    public function testCanGetDomainWithSubdomain(): void {
        $parser = new Parser();
        $d = $parser->domain("subdomain.domain.com");
        
        $this->assertTrue($d == "domain.com");

    }

    public function testCanGetDomainWithSubdomainAnd2LevelTLD(): void {
        $parser = new Parser();
        $d = $parser->domain("subdomain.domain.co.uk");
        
        $this->assertTrue($d == "domain.co.uk");

    }

    public function testCanGetDomainWithInsanity(): void {
        $parser = new Parser();
        $d = $parser->domain("www3.way.down.the.path.subdomain.domain.co.uk");
        
        $this->assertTrue($d == "domain.co.uk");

    }

}

?>